# submob

Dépôt du package submob. Ce package est dédié à l'import des [données de la base submob](https://nakala.fr/10.34847/nkl.847a6ad1#c8085faaa3b68cfda44e79d952cd63e3cc0c7ec8). La base submob est issue du projet Subwork. Elle est hébergée sur l'entrepôt Nakala.

## Installation

```         
# install.packages("remotes") 
remotes::install_git(url = "https://gitlab.huma-num.fr/tlecorre/submob")
```


A noter : l’import de certains fichiers fmpc peut prendre plusieurs secondes voire une à deux minutes.


